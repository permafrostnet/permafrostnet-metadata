# NSERC PermafrostNet Metadata Profile v1

This metadata profile is used for the [NSERC PermafrostNet ERDDAP data server](https://data.permafrostnet.ca/erddap). It adopts elements from the [ACDD](https://wiki.esipfed.org/Attribute_Convention_for_Data_Discovery_1-3#Conventions), [IOOS](https://ioos.github.io/ioos-metadata/ioos-metadata-profile-v1-2.html), and [CF](https://cfconventions.org/cf-conventions/cf-conventions.html) conventions to increase interoperability.

## Attributes


### Dataset description
Attribute | Name |  Description | Convention\* | Importance\*\*
:---------| :--------- | :------- | :------------------- | :-------:
`comments`|Comments|General comments on the dataset. This could include additional methods to the summary or any miscellaneous information.|ACDD|O
`date_modified`|Date modified|"The date on which the data was last modified. Note that this applies just to the data, not the metadata. The ISO 8601:2004 extended date format is recommended, as described in the Attributes Content Guidance section."|ACDD|M
`date_created`|Date published|"The date on which this version of the data was created. (Modification of values implies a new version, hence this would be assigned the date of the most recent values modification.) Metadata changes are not considered when assigning the date_created. The ISO 8601:2004 extended date format is recommended, as described in the Attribute Content Guidance section."|ACDD|M
`geospatial_lat_min`|Dataset minimum latitude|"The lower (most southern) latitude limit covered by the dataset. Use WGS84 / EPSG:4326 for the lat / long. Format is decimal degrees_north : [-90.000000, 90.000000]. For a specific point (e.g., a borehole), the Geospatial_lat_min and Geospatial_lat_max are the same."|ACDD|M
`geospatial_lat_max`|Dataset maximum latitude|"The higher (most northern) latitude limit covered by the dataset. Use WGS84 / EPSG:4326 for the lat / long. Format is decimal degrees_north : [-90.000000, 90.000000]. For a specific point (e.g., a borehole), the Geospatial_lat_min and Geospatial_lat_max are the same."|ACDD|M
`geospatial_lon_min`|Dataset minimum longitude|"The lowest (westernmost) longitude limit covered by the dataset. Use WGS84 / EPSG:4326 for the lat / long. Format is decimal degrees_east : [-180.000000, 180.000000]. For a specific point (e.g., a borehole), the Geospatial_lon_min and Geospatial_lon_max are the same."|ACDD|M
`geospatial_lon_max`|Dataset maximum longitude|"The highest (easternmost) longitude limit covered by the dataset. Use WGS84 / EPSG:4326 for the lat / long. Format is decimal degrees_east : [-180.000000, 180.000000]. For a specific point (e.g., a borehole), the Geospatial_lon_min and Geospatial_lon_max are the same."|ACDD|M
`geospatial_vertical_min`|Dataset minimum elevation|"The lowest altitude (in m) of the ground surface relative to sea level (geoid). If there is only one altitude, the same value should be recorded in geospatial_vertical_min and geospatial_vertical_max."|ACDD|R
`geospatial_vertical_max`|Dataset maximum elevation|"The highest altitude (in m) of the ground surface relative to sea level (geoid). If there is only one altitude, the same value should be recorded in geospatial_vertical_min and geospatial_vertical_max."|ACDD|R
`keywords`|Keywords|"Comma-separated list of keywords or phrases for the dataset. This can include keywords to describe the data but also the location where the data was collected. Ideally, use GCMD keyword structure (https://earthdata.nasa.gov/earth-observation-data/find-data/idn/gcmd-keywords); for a list of keywords including various categories, use the directory or the viewer (recommended). For location, we recommend using the list provided by the Polar Data Catalog (https://www.polardata.ca/pdcinput/public/keywordlibrary). Try to be as specific as possible with controlled keywords to maximize searchability."|ACDD|M
`keywords_vocabulary`|Keywords vocabulary|"If you are using a controlled vocabulary for the words/phrases in your 'keywords' attribute, this is the unique name or identifier of the vocabulary from which keywords are taken. If more than one keyword vocabulary is used, each may be presented with a prefix and a following comma, so that keywords may optionally be prefixed with the controlled vocabulary key. Example: 'GCMD:GCMD Keywords, CF:NetCDF COARDS Climate and Forecast Standard Names'."|ACDD|O
`location`|Location|"Name of the broader field location. Usually a region or sub-region. You can provide additional location names in the keywords section. Roughly corresponds to the GTN-P ""Site"" metadata attribute."|PN|O
`observation_depth_min`|Shallowest observation|"Depth in meters below the ground surface of the shallowest sensor or observation. For measurements made above the surface, the number should be negative."|PN|R
`observation_depth_max`|Deepest Observation|"Depth in meters below the ground surface of the deepest sensor or observation. For measurements made above the surface, the number should be negative."|PN|R
`processing_level`|Processing level|An explanation of what has been done on the data to validate and transform it.|ACDD|O
`project`|Project Name|"General name of the project. Projects usually span many years and involve multiple people. For instance, it can be an academic degree (e.g., Samuel Gagnon's master's degree project) or a larger project (e.g., Ground ice characterization in Nunavut for Polar Gas Pipeline)."|ACDD|O
`project_id`|Project ID|"Identifier of the project. Usually an alphanumerical identifier based on the project name. It is preferred to use only numbers (0-9), letters (A-Z), underscore (_), and hyphens (-). Avoid special characters (&, $, /, >, etc.) or blank spaces, and replace them with capitalized letters, underscore or hyphens."|PN|O
`summary`|Summary|"One paragraph describing the dataset. This is similar to an abstract, but only focused on the dataset, not necessarily the science and implications behind it. It should include why the dataset was obtained (the goal of the study), what was obtained (what is in the dataset), how it was obtained (a very short description of the methodology and the analyses performed if applicable), where (study site), and when (date of collection or duration of the time series)."|ACDD|M
`time_coverage_start`|Dataset start time|"Describes the time of the first data point in the dataset. Use the ISO 8601:2004 date format (see Data publication handbook for more information: https://drive.google.com/file/d/10Mw7Kx5Q8ZYv8VDspP1WGr8kINPHohh1/view?usp=sharing). This format corresponds to YYYY-MM-DDThh:mm:ss-00:00, where YYYY-MM-DD corresponds to year, month, and day, hh:mm:ss corresponds to the local time (hour, minutes, seconds) and –00:00 is timezone offset from UTC. The “T” in the middle is a marker for time. Time and full date are not mandatory, e.g. 2020-02-25 (for daily measurements) or 2020-02 (for monthly measurements)."|ACDD|M
`time_coverage_end`|Dataset end time|"Describes the time of the last data point in the dataset. Use the ISO 8601:2004 date format (see Data publication handbook for more information: https://drive.google.com/file/d/10Mw7Kx5Q8ZYv8VDspP1WGr8kINPHohh1/view?usp=sharing). This format corresponds to YYYY-MM-DDThh:mm:ss-00:00, where YYYY-MM-DD corresponds to year, month, and day, hh:mm:ss corresponds to the local time (hour, minutes, seconds) and –00:00 is timezone offset from UTC. The “T” in the middle is a marker for time. Time and full date are not mandatory, e.g. 2020-02-25 (for daily measurements) or 2020-02 (for monthly measurements)."|ACDD|M
`time_coverage_resolution`|Measurement frequency|"Describes the targeted time period between each value in the data set. Use ISO 8601:2004 duration format (yearly: P1Y, monthly: P1M, daily: P1D, hourly: PT1H, every 20 minutes: PT20M). For datasets that aren't measured regularly, leave this field blank. (see Data publication handbook p.13 for more information: https://drive.google.com/file/d/10Mw7Kx5Q8ZYv8VDspP1WGr8kINPHohh1/view?usp=sharing)."|ACDD|O
`title`|Title|"One sentence about the data contained within the file. The title generally includes the location"|ACDD|M


### Attribution and References
Attribute | Name |  Description | Convention\* | Importance\*\*
:---------| :--------- | :------- | :------------------- | :-------:
`acknowledgement`|Acknowledgements|"The list of funders, grant numbers and individual that participated in the creation of the dataset"|ACDD|O
`creator_name`|Name of the person who created the data|"The  name of the person principally responsible for collecting / creating the data. Only the name of one person should be here. Additional contributions can be provided in the contributor_name section. A person responsible for compiling or editing the dataset, but not the data itself, should be in the contributor section."|ACDD|M
`creator_email`|Email of the person who created the data|The email address of the person principally responsible for creating this data|ACDD|O
`creator_url`|URL of the person who created the data|"A link to the webpage of the person, lab or organisation responsible for collecting / creating the data."|ACDD|O
`contributor_name`|Names of contributors|"The names of the contributors or co-authors of the dataset, including the creator name. First name (First) and last name (Last) should be listed in the form First1 Last1,First2 Last2, etc."|ACDD|R
`contributor_role`|Contributor Roles|"The roles of the contributors or co-authors of the dataset. Chosen from the list at https://vocab.nerc.ac.uk/collection/G04/current/. The role of each contributor should be presented in the same order as the names in contributor_names and separated by commas."|ACDD|R
`contributor_url`|Contributor URLs|"URL of the personal website or ORCID number of the contributors or co-authors of the dataset, including the creator name. If a contributor does not have a URL, leave blank but separate with commas.","|IOOS|R
`contributor_email`|Contributor email|"Email addresses of the individuals or institutions that contributed to the creation of this data. Multiple emails should be separated by commas and presented in the same order and number as the names in contributor_names. For contributors with no email address, leave a blank (in the example, only the last contributor's email is provided)"|IOOS|O
`infoUrl`|Site information URL|"When available, the url for a resource that describes the station or provides real-time data (in the case of an online station). If not available, a link to another resource with more information. Can be the same as the metadata link"|IOOS|M
`institution`|Creator's institution|The name of the institution principally responsible for originating this data.|ACDD|O
`metadata_link`|Metadata link|A URL that gives the location of more complete metadata.|ACDD|O
`publisher_name`|Publisher|"The name of the person (or other entity specified by the publisher_type attribute) responsible for publishing the data file or product to users, with its current metadata and format."|ACDD|M
`publisher_type`|Publisher type|"Specifies type of publisher with one of the following: ['person', 'group', 'institution', or 'position']. If this attribute is not specified, the publisher is assumed to be a person."|ACDD|M
`references`|References|"Published references where this data is featured and/or where methods are explained. It can also be references to where the data was obtained (e.g., you are publishing data from a report). The references should include a DOI or a URL address if available. For multiple references, put each reference in between quotation marks and separate the references with commas."|ACDD|R


### Site characterization
These attributes are only used when the dataset represents a single location such as a borehole or sampling site.

Attribute | Name |  Description | Convention\* | Importance\*\*
:---------| :--------- | :------- | :------------------- | :-------:
`environment_description`|Site description|"Brief description of the environment. This can include information about geomorphological features (e.g., toundra polygons, pingo, etc.), topography, relative location (e.g., in a valley, on the top of a mountain, in a polygon trough), drainage (e.g., surface water visible, dry), vegetation (e.g., type, patterns), surface material (e.g., exposed bedrock,), and anything else visible at the surface."|PN|O
`ground_slope_angle`|Slope|Slope of the terrain in degrees (0-90°). Leave blank if unknown.|PN|O
`ground_slope_direction`|Slope direction|"Orientation in degrees of the steepest slope gradient (0-360° relative to the true North, 0° corresponds to the North, 90° East, 180° South, 270° West). Leave blank if unknown."|PN|O
`land_units`|Landforms represented|Comma-separated list of landscape units or landforms in which the observations were performed. Chose from the [CPERS landform list](permafrostnet.gitlab.io/vocabularies/vocabs/landforms/CPERSLF.html). Leave blank if unknown.|PN|O
`organic_matter_thickness`|Organic matter thickness|"Thickness (in meters) of the surface organic cover, including the peat, moss, litter, lichen and decomposing organic matter. Leave blank if unknown."|PN|O
`overburden_thickness`|Overburden thickness|Thickness (in meters) of the mineral overburden (or depth until bedrock is reached). Leave blank if unknown.|PN|O
`platform_pitch_fore_down`|borehole dip|"(Only for borehole data) Indicate the dip of the borehole, i.e. indicate the angle of the inclination (in degrees) measured with regard to the ground, so a vertical borehole is 90°."|PN|O
`platform_orientation`|borehole azimuth|"(Only for borehole data). If the hole or borehole has a dip, i.e., is inclined, indicate the orientation (0-360° relative to the true North, 0° corresponds to the North, 90° East, 180° South, 270° West) of the inclination. If the borehole dip is 90 or if the dataset does not represent a borehole, this field can be left blank."|PN|O
`permafrost_status`|Permafrost conditions|"Whether permafrost is present. Chosen from [absent, not suspected (unconfirmed), suspected (unconfirmed), present, and not recorded ]"|PN|O
`platform_name`|Site name|"The name of a borehole meteorological station, site, etc. It is the most specific identifier given to the location, and will usually be the same as the borehole name in the case of a borehole. The name should be unique to a project. It is useful to use a combination of letters and numbers to increase uniqueness."|IOOS|R
`platform_id`|Site ID|"The platform ID is an identifier used to cross-reference datasets pertaining to the same location. In the case of boreholes, it will often be the same as the borehole ID. The platform ID should preferably be related to the platform name. This is particularly important if multiple datasets are from the same location (e.g., multiple boreholes from the same study site)."|IOOS|R
`platform`|Type of observation|"Indicate if the data was recorded from a ["borehole", "soil pit", "outcrop", "surface instrumentation"]|ACDD|O
`surface_cover`|Surface cover|"Select the dominant type of surface cover from [exposed bedrock, vegetated, water, exposed soil (coarse), exposed soil (fine), blocky material, unknown.]. In the case of multiple locations, select the dominant type found. In this example, "exposed soil (coarse)" implies coarse sands and coarser and "exposed soil (fine)" implies sands and finer."|PN|O
`surficial_geology`|Surficial Geology|"Select the dominant surface material at the site from [alluvial, bedrock, colluvial, glacial (till), glaciofluvial, lacustrine, marine, organic, aeolian, beach, coarse lag, unknown]. In the case of multiple locations, select the dominant type found. Terms modified from Fulton (1995). See [this vocabulary list]](https://wwww.permafrostnet.gitlab.io/vocabularies/vocabs/fulton95/fulton95.html) for definitions."|PN|O
`vegetation_type`|Vegetation type|"Select the dominant vegetation type among [ coniferous forest, deciduous forest, graminoids (grasses), herbaceous (non-woody), lichen, moss, shrub, no vegetation, not recorded ]"|PN|O

### Technical data
These are used by the ERDDAP server or are requirements for other conventions.

Attribute | Name |  Description | Convention\* | Importance\*\*
:---------| :--------- | :------- | :------------------- | :-------:
`id`|Identifier|"An identifier for the data set, provided by and unique within its naming authority. The combination of the ""naming authority"" and the ""id"" should be globally unique, but the id can be globally unique by itself also. IDs can be URLs, URNs, DOIs, meaningful text strings, a local key, or any other unique string of characters. The id should not include white space characters. "|ACDD|M
`Conventions`|Conventions|A comma-separated list of the conventions that are followed by the dataset. |CF|R
`featureType`|Feature Type|"CF attribute for identifying the featureType. Chosen from: ""point"", ""profile"", ""timeSeries"", ""timeSeriesProfile"", ""trajectory"". Typically ""timeSeries"" or ""timeSeriesProfile"" is used for ground temperature data and ""profile"" is used for borehole or soil profile data."|CF|M
`naming_authority`|Naming Authority|The organization that provides the initial id (see above) for the dataset. The naming authority should be uniquely specified by this attribute. We recommend using reverse-DNS naming for the naming authority; URIs are also acceptable. Example: 'edu.ucar.unidata'.|ACDD|R



> `*` **Convention** 
> * `ACDD`: [Attribute Convention for Data Discovery](https://wiki.esipfed.org/Attribute_Convention_for_Data_Discovery_1-3#Conventions)
> * `IOOS`: [Integrated Ocean Observing System](https://ioos.github.io/ioos-metadata/ioos-metadata-profile-v1-2.html)
> * `CF`: [Climate and Forecast](https://cfconventions.org/cf-conventions/cf-conventions.html)
> * `PN`: PermafrostNet (this convention)

> `**` **Importance** 
> * **M** Mandatory
> * **R** Recommended
> * **O** Optional


